import { useState } from 'react';
import './App.css';

function App() {
  const [count, setCount] = useState(0);
  const [input, setInput] = useState("");
  const [error, setError] = useState(null)
  const [list, setList] = useState([]);
  return (
    <div className="App">
      <div>
        {count}
      </div>
      <button onClick={() => setCount(count + 1)}>
        +
      </button>
      <button onClick={() => setCount(count - 1)}>
        -
      </button>
      <form onSubmit={handleSubmit}>
        {machineEmptyError()}
        <label>
          Add machine...
          <input value={input} onChange={handleInputChange}></input>
        </label>
        <button type="submit">
          Add
        </button>
      </form>
      <div>
        {list.map((machine, index) => <p key={index} className='MachineName'>{machine}</p>)}
      </div>
    </div>
  );

  function handleInputChange(event) {
    setInput(event.target.value)
    setError(null)
  }

  function handleSubmit(event) {
    event.preventDefault()
    if(input.length > 0) {
      setList(list.concat(input))
    }
    else {
      setError("Machine name cannot be empty")
    }
  }

  function machineEmptyError() {
    if(error) {
      return <p>
        {error}
      </p>
    }else {
      return null
    }
  }
}

export default App;
