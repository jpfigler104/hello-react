import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from './App';

test('number starts at 0', () => {
  render(<App />);
  expect(screen.queryByText('0')).toBeInTheDocument()
});

test('plus button increments number', () => {
  render(<App />);
  const plusButton = screen.getByRole('button', {name:'+'});
  fireEvent.click(plusButton)
  expect(screen.queryByText('1')).toBeInTheDocument()
  expect(screen.queryByText('0')).not.toBeInTheDocument()
});

test('type into input field, add value to list', () => {
  render(<App />);
  const input = screen.getByLabelText('Add machine...')
  userEvent.type(input, 'Banzai Run')
  const addButton = screen.getByRole('button', {name:'Add'})
  fireEvent.click(addButton)
  expect(screen.queryByText('Banzai Run')).toBeInTheDocument()
})

test('display error when attempting to submit empty value', () => {
  render(<App/>);
  const addButton = screen.getByRole('button', {name:"Add"})
  fireEvent.click(addButton)
  expect(screen.queryByText('Machine name cannot be empty')).toBeInTheDocument()
  const input = screen.getByLabelText('Add machine...')
  userEvent.type(input, 'test')
  expect(screen.queryByText('Machine name cannot be empty')).not.toBeInTheDocument()
})

test('minus button decrements number, expects values < 0', () => {
  render(<App/>);
  const minusButton = screen.getByRole('button', {name:'-'})
  fireEvent.click(minusButton)
  expect(screen.queryByText('-1')).toBeInTheDocument()
  expect(screen.queryByText('0')).not.toBeInTheDocument()
})